# WP commands for driv-cli

Commands for WordPress development etc.

1. Add `git@bitbucket.org:drivdigital/driv-cli-wp.git` to your `${DRIV-CLI-HOME}/repositories.json`
2. Start driv-cli and run the `driv update` command.