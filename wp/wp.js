"use strict";

module.exports = function ( cli, options ) {

    cli
        .command( 'wp plugin', 'Menu' )
        .action( function ( args, callback ) {
            let Project = require( './lib/classes/Project' );
            if ( !Project.has_build_json_file() ) {
                Driv.warn( 'build.json file not found' );
                callback();
                return;
            }

            Driv.log( 'WORDPRESS PLUGIN BUILDER' );
            Driv.log( '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~' );
            Driv.log( 'Project' );
            Driv.log( '@dir:         ' + Driv.get_current_working_dir() );
            Driv.log( '@Name:        ' + Project.get_name() );
            Driv.log( '@Stable tag:  ' + Project.get_stable_tag() );
            Driv.log( '' );

            this.prompt( [
                {
                    name: 'target',
                    type: 'list',
                    message: 'Select a target',
                    choices: [
                        {
                            name: 'Zip          Create zip file',
                            value: 'zip'
                        },
                        {
                            name: 'Changelog    Read the changelog',
                            value: 'changelog'
                        },
                        {
                            name: 'Release      Push to Wordpress.org',
                            value: 'release'
                        },
                        {
                            name: 'Filters      Generate a list of filters',
                            value: 'filters'
                        },
                        {
                            name: 'Clean        Clean project',
                            value: 'clean'
                        }
                    ]
                }

            ],  ( result ) => {
                load_target( result.target );
                callback();
            } );

            callback();
        } );

    cli
        .command( 'wp plugin zip', 'Create zip file' )
        .action( ( args, callback ) => {
            load_target( 'zip' );
            callback();
        } );

    cli
        .command( 'wp plugin changelog', 'Read the changelog' )
        .action( ( args, callback ) => {
            load_target( 'changelog' );
            callback();
        } );

    cli
        .command( 'wp plugin release', 'Release' )
        .action( function ( args, callback ) {
            let Project = require( './lib/classes/Project' );
            let message = 'Release';
            if ( Project.has_build_json() ) {
                message += ' ' + Project.get_stable_tag();
            }
            this.prompt( [{
                type: 'confirm',
                name: 'confirm',
                message: message

            }], ( res ) => {
                if ( res.confirm ) {
                    load_target( 'release' );
                }
            } );

            callback();
        } );

    cli
        .command( 'wp plugin filters', 'Display filters created by the plugin' )
        .action( () => load_target( 'filters' ) );

    cli
        .command( 'wp plugin clean', 'Clean the project' )
        .action( () => load_target( 'clean' ) );

};

function load_target( target ) {
    global.WP_BUILD_DIR = pwd() + '/.build';
    global.WP_ZIP_DIR = global.WP_BUILD_DIR + '/zip';

    let Project = require( './lib/classes/Project' );
    let _pwd = pwd();

    if ( !Project.has_build_json_file() ) {
        Driv.warn( 'build.json file not found' );
        return;
    }

    let target_file = './lib/' + target;

    require( target_file );

    cd( _pwd );

    delete require.cache[require.resolve( target_file )];
}
