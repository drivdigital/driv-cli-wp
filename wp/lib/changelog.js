/*global module, require, test, mkdir, rm, cd, echo, exec, ls, sed, cp */

"use strict";

require( 'shelljs/global' );
let Project = require( './classes/Project' );

Project.set_up();

let readme = cat( Project.get_git_dir() + '/readme.txt' );
let parts = readme.split( '== Changelog ==' );

let lines = parts[1].trim().split( '\n' );
Driv.log( '  == Changelog ==\n');

for ( let line of lines ) {
    Driv.log( '  ' + line );
}

Driv.log( '\n' );
