/*global module, require, test, mkdir, rm, cd, echo, exec, ls, sed, cp */

"use strict";

require( 'shelljs/global' );
let Project = require( './classes/Project' );

Project.set_up();

let zip_file = Project.get_name() + '_' + Project.get_stable_tag() + '_' + Utils.format_date( new Date(), 'YYYY-MM-DD-HHMMSS' ) + '.zip';

let name = Project.get_name();

cd( WP_BUILD_DIR );

exec( 'zip -r ' + WP_ZIP_DIR + '/' + zip_file + ' ./' + name, {silent: false} );

Driv.log( 'Done' );