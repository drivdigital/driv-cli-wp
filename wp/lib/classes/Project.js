/*global module, require, test, mkdir, rm, cd, echo, exec, ls, sed */

"use strict";

require( "shelljs/global" );
let Validator = require( './Validator' );

class Project {
    static set_up() {

        //if ( !Project.has_build_json() ) {
        //
        //    Driv.error( 'build.json does not exist.' );
        //
        //    //vorpal.prompt( [
        //    //    {
        //    //        name:    'stable_tag',
        //    //        message: 'Stable tag'
        //    //    }
        //    //], function ( result ) {
        //    //    if ( test( '-d', '.git' ) ) {
        //    //        let git_url = exec( 'git config --get remote.origin.url', {silent: true} ).output.trim();
        //    //        if ( git_url ) {
        //    //            let name = Utils.get_last_part_of_path( git_url ).split( '.' )[0];
        //    //            let o = {
        //    //                name: name,
        //    //                stable_tag: result_stable_tag,
        //    //                git_repo: git_url,
        //    //                wp_repo: '' //https://plugins.svn.wordpress.org/
        //    //            };
        //    //            JSON.stringify( o, null, 4 ).to( Project.get_build_json_file() );
        //    //        }
        //    //    }
        //    //} );
        //
        //    //Driv.log( cat( Project.get_build_json_file() ) );
        //    exit( 1 );
        //}

        let git_dir = Project.get_git_dir();
        if ( test( '-d', WP_BUILD_DIR ) ) {
            rm( '-rf', WP_BUILD_DIR );
        }
        mkdir( '-p', WP_BUILD_DIR );
        mkdir( '-p', WP_ZIP_DIR );
        mkdir( '-p', git_dir );

        Project.clone();

        let v = new Validator( git_dir );
        v.init();
        if ( !v.is_valid() ) {
            Driv.error( 'Errors found in readme.txt' );
            exit( 1 );
        }

        Project.replace_macros();
    }

    static clone() {
        let repo = Project.get_git_repo();
        let git_dir = Project.get_git_dir();

        exec( 'git clone ' + repo + ' ' + git_dir );

        rm( '-rf', git_dir + '/.git' );
        rm( git_dir + '/.gitignore' );
    }

    static replace_macros() {
        let stable_tag = Project.get_stable_tag();
        let git_dir = Project.get_git_dir();

        ls( '-R', git_dir + '/*' ).forEach( function ( file ) {
            if ( !test( '-d', file ) ) {
                sed( '-i', /##VERSION##/g, stable_tag, file );
            }
        } );
    }

    static get_git_dir() {
        return WP_BUILD_DIR + '/' + Project.get_name();
    }

    static has_build_json_file() {
        return test( '-f', Driv.get_current_working_dir() + '/build.json' );
    }

    static get_build_json_file() {
        return Driv.get_current_working_dir() + '/build.json';
    }

    static get_build_json() {
        return JSON.parse( cat( Project.get_build_json_file() ) );
    }

    static get_stable_tag() {
        return Project.get_build_json().stable_tag;
    }

    static get_name() {
        return Project.get_build_json().name;
    }

    static get_git_repo() {
        return Project.get_build_json().git_repo;
    }

    static get_wp_repo() {
        return Project.get_build_json().wp_repo;
    }

    static has_build_json() {
        return test( '-f', Project.get_build_json_file() );
    }
}

module.exports = Project;

