/*global module, require, test, mkdir, rm, cd, echo, exec, ls, sed, cat */

"use strict";

require( "shelljs/global" );

class FileNotFoundException {
    constructor( message ) {
        this.message = message;
        this.name = 'FileNotFoundException';
    }
}

class Validator {
    constructor( path_to_project ) {
        this.errors = 0;
        this.warnings = 0;
        this.project_path = path_to_project;

        if ( !test( '-f', this.project_path + '/readme.txt' ) ) {
            throw new FileNotFoundException( 'Project should have a readme.txt file' );
        }
        this.readme = cat( this.project_path + '/readme.txt' );

        if ( !test( '-f', this.project_path + '/license.txt' ) ) {
            throw new FileNotFoundException( 'Project should have a license.txt file' );
        }

        this.readme = cat( this.project_path + '/readme.txt' );
    }

    init() {
        echo( '\n  Validating readme.txt\n' );

        if ( !this.has_plugin_name() ) {
            this.log( '[Error] No plugin name detected. Plugin names look like: === Plugin Name ===' );
            this.errors++;
        }

        for ( let f of this.get_required_fields() ) {
            if ( !this.has_field( f + ':' ) ) {
                this.log( '[Error] Has not ' + f + ' field. Should look like ' + f + ': ');
                this.errors++;
            }
        }

        let sections = this.find_readme_sections();
        for ( let s of this.get_required_sections() ) {
            if ( sections.indexOf( s ) == -1 ) {
                this.log( '[Error] No ' + s + ' section found. Should look like: == ' + s + ' ==' );
                this.errors++;
            }
        }
        for ( let s of this.get_arbitrary_sections() ) {
            if ( sections.indexOf( s ) == -1 ) {
                this.log( '[Warning] No ' + s + ' section found. Should look like: == ' + s + ' ==' );
                this.warnings++;
            }
        }
        echo( '' );
    }

    is_valid() {
        return this.errors == 0;
    }

    has_plugin_name() {
        let re = /^===(\s.+)===$/gm;
        return re.test( this.readme );
    }

    get_required_sections() {
        return [
            'Description',
            'Installation',
            'Frequently Asked Questions',
            'Changelog'
        ];
    }

    get_arbitrary_sections() {
        return [
            'Screenshots'
        ]
    }

    get_required_fields() {
        return [
            'Author',
            'Author URI',
            'Contributors',
            'Tags',
            'Requires at least',
            'Tested up to',
            'Stable tag',
            'License',
            'License URI'
        ];
    }

    find_readme_sections() {
        let result = [];
        let re = /^==(\s.+)==$/gm;
        let item;

        while ( item = re.exec( this.readme ) ) {
            result.push( item[1].trim() );
        }
        return result;
    }

    has_field( field_name ) {
        let re = new RegExp( '^' + field_name, 'gm' );

        return re.test( this.readme );
    }

    log( msg ) {

        echo( '    * ' + msg );

    }

}

module.exports = Validator;
