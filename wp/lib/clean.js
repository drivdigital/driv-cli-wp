/*global module, require, test, mkdir, rm, cd, echo, exec, ls, sed */

"use strict";

require( 'shelljs/global' );
let Project = require( './classes/Project' );
cd( Driv.get_current_working_dir() );

if ( test( '-d', WP_BUILD_DIR ) ) {
    rm( '-rf', WP_BUILD_DIR );
}

Driv.log( 'Done' );