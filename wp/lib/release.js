/*global module, require, test, mkdir, rm, cd, echo, exec, ls, sed, cp*/

"use strict";

require( 'shelljs/global' );

let Project = require( './classes/Project' );
let wp_repo = Project.get_wp_repo();
let stable_tag = Project.get_stable_tag();
let svn_dir = WP_BUILD_DIR + '/wordpress.org';

if ( !wp_repo ) {
    Driv.error('wp_repo not defined in build.json');
    exit(1);
}

Project.set_up();

mkdir( svn_dir );

// SVN Checkout the wordpress.org repo.
Driv.log( 'Checking out ' + wp_repo );
exec( 'svn co ' + wp_repo + ' ' + svn_dir );

// Exit if tag already exist.
if ( test( '-d', svn_dir + '/tags/' + stable_tag ) ) {
    Driv.warn( 'SVN tag ' + stable_tag + ' already exists.' );
    exit( 1 );
}

rm( '-rf', svn_dir + '/trunk/*' );

cd( Project.get_git_dir() );
cp( '-rf', '.', svn_dir + '/trunk' );

cd( svn_dir );

// Add and delete files from the repo.
let status_text = exec( 'svn status', {silent: true} ).output;
var status_lines = status_text.split( '\n' );
status_lines.forEach( function ( line ) {
    if ( line.trim() != '' ) {
        let parts = line.split( /\s+/ );
        let status = parts[0];
        let file = parts[1];
        if ( status == '?' ) {
            exec( 'svn add ' + file );
        }
        if ( status == '!' ) {
            exec( 'svn delete ' + file );
        }
    }
} );

// Create version.
exec( 'svn cp trunk tags/' + stable_tag );

// Commit.
Driv.log( 'Committing to ' + Project.get_wp_repo());

//exec( 'svn commit -m "Release ' + stable_tag + '"' );

Driv.log( 'Done' );