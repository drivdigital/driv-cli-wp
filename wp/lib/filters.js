/*global module, require, test, mkdir, rm, cd, echo, exec, ls, sed, cp */

"use strict";

require( 'shelljs/global' );
let Project = require( './classes/Project' );

Project.set_up();

let re = /apply_filters\s*\(\s*['"](.+)['"]/gm;
let result = [];
ls( '-R', Project.get_git_dir() + '/*' ).forEach( function ( file ) {
    if ( !test( '-d', file ) ) {
        let content = cat( file );
        let matches = get_matches( content, re );
        if ( matches.length ) {
            for ( let m of matches ) {
                result.push( m );
            }
        }
    }
} );

for (let r of result ) {
    Driv.warn ('* ' + r);
}

function get_matches( string, regex, index ) {
    index || (index = 1); // default to the first capturing group
    var matches = [];
    var match;
    while ( match = regex.exec( string ) ) {
        matches.push( match[index] );
    }
    return matches;
}
